import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { PostService } from '../post/post.service';
import { Post } from '../post/post.model';

@Component({
  selector: 'add-post',
  templateUrl: './add-post.component.html',
  styleUrls: ['./add-post.component.scss']
})
export class AddPostComponent implements OnInit {

  post: Post = new Post();
  submitted = false;
 
  constructor(private postService: PostService) {}
 
  ngOnInit() {
  }
 
  newPost(): void {
    this.submitted = false;
    this.post = new Post();
  }
 
  save() {
    this.postService.createPost(this.post);
    this.post = new Post();
  }
 
  onSubmit() {
    this.submitted = true;
    this.save();
  }
}
