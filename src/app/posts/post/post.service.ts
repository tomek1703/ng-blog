import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database'
import { Post } from './post.model';


@Injectable()

export class PostService {

  private dbPath = '/posts';

  postsRef: AngularFireList<Post> = null;

  constructor( private db: AngularFireDatabase ) {
    this.postsRef = db.list(this.dbPath);
  }

  createPost(post: Post): void {
    this.postsRef.push(post);
  }

  updatePost(key: string, value: any): void {
    this.postsRef.update(key, value).catch(error => this.handleError(error));
  }

  deletePost(key: string): void {
    this.postsRef.remove(key).catch(error => this.handleError(error));
  }

  getPosts(): AngularFireList<Post> {
    return this.postsRef;
  }

  private handleError(error) {
    console.log(error);
  }
} 
