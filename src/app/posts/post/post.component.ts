import { Component, OnInit, Input } from '@angular/core';
import { PostService } from './post.service';
import { Post } from '../post/post.model';

@Component({
  selector: 'post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit {

  @Input() post: Post;

  constructor( private postService: PostService) { }

  ngOnInit() {
  }

  // editPost(){
  //   this.postService.updatePost(this.post.key,)
  // }
  deleteCustomer(){
    this.postService.deletePost(this.post.key);
  }

}
