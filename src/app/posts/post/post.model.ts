export class Post {
    key : string;
    title: string;
    text: string;
    author: string;
}