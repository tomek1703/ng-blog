import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AngularFireModule } from 'angularfire2';
import { FormsModule } from '@angular/forms';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { environment } from '../environments/environment';
import { HeaderComponent } from './header/header.component';
import { PostsComponent } from './posts/posts.component';
import { FooterComponent } from './footer/footer.component';
import { ContactComponent } from './contact/contact.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { OopsComponent } from './oops/oops.component';
import { PostComponent } from './posts/post/post.component';
import { PostEditComponent } from './posts/post/post-edit/post-edit.component';
import { AddPostComponent } from './posts/add-post/add-post.component';
import { PostService } from './posts/post/post.service';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'blog', component: PostsComponent },
  { path: 'about', component: AboutComponent },
  { path: 'contact', component: ContactComponent },
  { path: 'blog/add-post', component: AddPostComponent },
  { path: '**', component: OopsComponent }
  
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    PostsComponent,
    FooterComponent,
    ContactComponent,
    HomeComponent,
    AboutComponent,
    OopsComponent,
    PostComponent,
    PostEditComponent,
    AddPostComponent
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase),
    RouterModule.forRoot(routes),
    CollapseModule.forRoot(),
    AngularFireDatabaseModule,
    FormsModule,
  ],
  providers: [ PostService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
